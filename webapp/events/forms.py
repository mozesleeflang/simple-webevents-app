# webapp/events/forms.py
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextField, IntegerField,DateTimeField
from wtforms.validators import DataRequired


class NewEventForm(FlaskForm):
    sname = StringField("Name", [DataRequired()])
    description = TextField("Description", [DataRequired()])
    planned = DateTimeField("Date", [DataRequired()])
    submit = SubmitField("Add Event")


class RemoveEventForm(FlaskForm):
    event_id = IntegerField("Event ID", [DataRequired()], 'Provide Event id')
    event_name = StringField("Event", [DataRequired()], 'Cannot remove an event without knowing the name')
    submit = SubmitField("Remove Event")